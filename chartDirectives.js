// AngularJS module for displaying and updating NVD3 charts
// Author: Kjartan B. Kristjansson
// Owner:  Silverberg Technologies

( function() {
        'use strict';

        angular.module('chartDirectives', []).directive('lineChart', function() {
                return {
                        restrict: 'E',
                        template: '<div id="chart{{chartid}}"><svg></svg></div>',
                        scope: {
                                chartData: '=data',
                                chartid: '@'
                        },
                        link: function(scope, element, attrs) {
                                scope.$watch('chartData', function() {
                                        if(scope.chart) {
                                                // If chart already exists, update it and return.
                                                return scope.chart.update();
                                        }
                                        // Create new chart
                                        nv.addGraph(function() {
                                                var chart = nv.models.lineChart()
                                                        .margin({left: 100})
                                                        .showLegend(true)
                                                        .showYAxis(true)
                                                        .showXAxis(true);
                                                
                                                chart.xAxis.axisLabel(attrs.xlabel === undefined ? 'x axis' : attrs.xlabel);
                                                chart.xAxis.tickFormat(attrs.xformat === undefined ? d3.format(',r') : d3.format(attrs.xformat));
                                                chart.yAxis.axisLabel(attrs.ylabel === undefined ? 'y axis' : attrs.ylabel);
                                                chart.yAxis.tickFormat(attrs.yformat === undefined ? d3.format(',r') : d3.format(attrs.yformat));

                                                d3.select('#chart'+scope.chartid+' svg').datum(scope.chartData).call(chart);
                                                nv.utils.windowResize(function() {
                                                        chart.update();
                                                });
                                                scope.chart = chart;
                                                return chart;
                                        });
                                }, true);
                        }
                };
        });

}() );
