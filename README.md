## AngularJS directives for displaying NVD3 charts ##

### Requirements ###
* AngularJS
* D3
* NVD3

See [wiki](/silverberg/chart-directives/wiki/) for further instructions.

### Maintainers ###
* Kjartan Kristjánsson

